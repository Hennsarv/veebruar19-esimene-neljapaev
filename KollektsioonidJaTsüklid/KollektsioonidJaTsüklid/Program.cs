﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KollektsioonidJaTsüklid
{
    class Program
    {
        static void Main(string[] args)
        {

            // lisaks massiividele veel mõned asjad

            List<int> arvud = new List<int>();
            arvud.Add(7);
            arvud.Add(8);
            arvud.Add(3);
            arvud.Add(2);
            arvud.Add(1);
            arvud.Add(4);
            arvud.Remove(3);

            // listil on olemas
            //  index arvud[i]
            //  Add(n) lisab listi n
            //  Remove(n) leiab listist ja kustutab (ühe) n-i
            //  RemoveAt(i) kustutab listist i-nda elemendi
            //  Count (elementide arv)
            //  Capacity (maht, enne kui kolima peab)

            Console.WriteLine("\nsiit hakkab järjekord\n");
            // järjekord - esimesena sisse esimesena välja (FIFO)
            Queue<int> järjekord = new Queue<int>();
            järjekord.Enqueue(1);
            järjekord.Enqueue(2);
            järjekord.Enqueue(7);
            
            // järjekorral on
            //  index järjekord[i]
            //  Enqueue(n) - pistab järjekorda
            //  Dequeue() - võtab järjekorrast
            //  Peek() - piilub järjekorras järgmist
            //  ... jpm

            while(järjekord.Count > 0)
                Console.WriteLine(järjekord.Dequeue());

            Console.WriteLine("\nsiit hakkab kuhi\n");

            Stack<int> kuhi = new Stack<int>();
            kuhi.Push(1);
            kuhi.Push(2);
            kuhi.Push(7);

            while (kuhi.Count > 0)
                Console.WriteLine(kuhi.Pop());

            // kuhjal on 
            //  index - kuhi[i]
            //  Push(n) - pane kuhja peale n
            //  Pop() - võta kuhjast oealmine
            //  Peek() - piilu, kes on pealmine

            // eile oli kaks
            // for ja foreach

            // täna õpime veel kaks
            // while ja do while

            Console.WriteLine("\nVeel üks vigur - SortedSet\n");
            Console.WriteLine("paneme sinna: { 1,3,6,2,4,7,3,8,1}");
            SortedSet<int> veelArvud = new SortedSet<int>()
            { 1,3,6,2,4,7,3,8,1};

            foreach (var x in veelArvud) Console.WriteLine(x);

            Console.WriteLine("\nVeel üks vigur - HashSet\n");
            Console.WriteLine("paneme sinna: { 1,3,6,2,4,7,3,8,1}");
            HashSet<int> teisedArvud = new HashSet<int>()
            { 1,3,6,2,4,7,3,8,1};

            foreach (var x in teisedArvud) Console.WriteLine(x);


            Console.WriteLine("\nalguses tegime mingid arvud:\n");
            foreach (var x in arvud) Console.WriteLine(x);

            Console.WriteLine("\neelmised arvud aga sorteerituna\n");
            arvud.Sort();
            foreach (var x in arvud) Console.WriteLine(x);

            // ma teen veel ka ühe capacity näite
            Console.WriteLine("\nCapacity ja list\n");
            List<int> veel = new List<int>();
            Console.WriteLine($"Alguses on count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");
            veel.Add(1); Console.WriteLine($"count={veel.Count} ja maht={veel.Capacity}");

            // siis tulevad veel Dictionary ja SortedList jne...


        }
    }
}
