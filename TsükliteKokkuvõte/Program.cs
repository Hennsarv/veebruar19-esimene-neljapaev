﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TsükliteKokkuvõte
{
    class Program
    {
        static void Main(string[] args)
        {
            // for - kõigi tsüklite isa
            Console.WriteLine("\nFOR-tsükkel:\n");

            for (int i = 0; i < 10; i++) { Console.WriteLine(i); } // i on tsüklimuutuja

            // while 

            Console.WriteLine("\nWhile-tsükkel:\n");

            int arv = 0;
            while (arv < 10) { Console.WriteLine(arv++); }

            Console.WriteLine("\nsama FOR-iga:\n");
            for (; arv < 20;) { Console.WriteLine(arv++); }

            // do while

            Console.WriteLine("\nDO-tsükkel:\n");
            do { Console.WriteLine(arv++); } while (arv < 30);

            Console.WriteLine("\nsama FOR-iga:\n"); // ära seda loe
            for (bool b = true; b; b = arv < 40) { Console.WriteLine(arv++); }

            // foreach

            Console.WriteLine("\nFOREACH-tsükkel:\n");

            int[] arvud = { 1, 2, 3, 4, 5 };
            foreach (var x in arvud) { Console.WriteLine(x); }

            Console.WriteLine("\nsama FOR-iga:\n"); //seda ära loe - ajab segadusse vaid
            for (var ie = arvud.GetEnumerator(); ie.MoveNext();)
            {
                var x = ie.Current;
                Console.WriteLine(x);
            }


        }
    }
}
