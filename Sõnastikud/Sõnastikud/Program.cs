﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sõnastikud
{
    class Program
    {
        static void Main(string[] args)
        {
            // pisut keerulisem kollektsioon - Dictionary
            var nimekiri = new SortedList<string, int>();
            nimekiri.Add("Henn", 63);
            nimekiri.Add("Ants", 40);
            nimekiri.Add("Peeter", 28);
            nimekiri.Add("Jaak", 17);

            Console.WriteLine(nimekiri["Ants"]);
            nimekiri["Jaak"]++;

            nimekiri.Add("Juku", nimekiri["Jaak"]);
            nimekiri.Remove("Jaak");

            Console.WriteLine("Meil on sellised nimed:");
            foreach (var x in nimekiri.Keys) Console.WriteLine($"\t{x}");
            Console.Write("Kelle vanust sa tahad: ");
            string nimi = Console.ReadLine();

            if (nimekiri.ContainsKey(nimi))
                Console.WriteLine(nimekiri[nimi]);
            else
                Console.WriteLine("sihukest ma ei tunne");

            // Dictionary
            // SortedDictionary
            // SortedList

            foreach(var x in nimekiri)
                Console.WriteLine($"{x.Key} on {x.Value} aastat vana");



            Console.WriteLine(nimekiri.Values.Average());
            Console.WriteLine(nimekiri.Values.Max());
            Console.WriteLine(nimekiri.Values.Min());
        }
    }
}
