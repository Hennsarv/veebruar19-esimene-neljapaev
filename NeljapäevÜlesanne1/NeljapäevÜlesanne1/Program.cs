﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeljapäevÜlesanne1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> arvud = new List<int>();
            string rida; // = "*";
            do
            //for(string rida = "*"; rida != "";)
            {
                Console.Write("anna üks arv: ");
                rida = Console.ReadLine();
                if (rida != "")
                {
                    arvud.Add(int.Parse(rida));
                }
            } while (rida != "") ;

                if (arvud.Count > 0)
                Console.WriteLine(arvud.Average());
            else Console.WriteLine("pole arvu pole keskmist");

            // teine võimalik lahendus
            double summa = 0;
            int mitu = 0;
            do
            {
                Console.Write("anna veel üks arv: ");
                rida = Console.ReadLine();
                if (rida != "")
                {
                    if (double.TryParse(rida, out double arv))
                    {
                        summa += arv;
                        mitu++;

                    }
                    else Console.WriteLine("see pole miski arv");
                }
            } while (rida != "");

            Console.WriteLine(summa / mitu);

        }
    }
}
